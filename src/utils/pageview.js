function TrackPageView(url) {
  try {
    window.gtag('config', 'UA-47264711-1', {
      page_location: url
    });
  } catch (error) {
    // silences the error in dev mode
    // and/or if gtag fails to load
  }
return TrackPageView;
}
