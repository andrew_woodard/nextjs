import React from "react";

import {
  CardSidebar,
  Block,
  TitleSidebar,
  TitlePost,
  Date,
  CatList,
  CatListItem,
} from "../../components/Sidebar";

import InputSearch from "../../components/InputSearch";

const Sidebar = () => {
  return (
    <>
      <CardSidebar p="15px" pl="20px">

        <form>
          <InputSearch />
        </form>

      </CardSidebar>
      <CardSidebar>
        <TitleSidebar>Recent Posts</TitleSidebar>
        <Block>
          <TitlePost>

          </TitlePost>
          <Date></Date>
        </Block>

      </CardSidebar>
    </>
  );
};
export default Sidebar;
