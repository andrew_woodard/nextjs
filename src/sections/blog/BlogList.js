import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { FaAngleRight, FaAngleLeft } from "react-icons/fa";
import Link from "next/link";

import { Title, Section, Box } from "../../components/Core";
import PostCard from "../../components/PostCard";
import Pagination, { PageItem } from "../../components/Pagination";

import imgB1 from "../../assets/image/jpeg/integritassmall.jpg";
import imgB2 from "../../assets/image/jpeg/fusionbrands.jpg";
import imgB3 from "../../assets/image/jpeg/morrellbuilderssmall.jpg";
import imgB4 from "../../assets/image/jpeg/mannsshot.webp";
import imgB5 from "../../assets/image/jpeg/morrcomm.jpg";
import imgB6 from "../../assets/image/jpeg/myincontrolsmall.jpg";
import imgB7 from "../../assets/image/jpeg/cinsider.jpg"; 

const BlogList = () => (
  <>
    {/* <!-- Blog section --> */}
    <Section className="position-relative">
      <Container>
        <Row className="align-items-center justify-content-center">
          <Col lg="4" className="mb-5">
            <PostCard
              img={imgB1}
              preTitle="Oct 4, 2020"
              title="Creating a technology focused website on Jekyll!"
              readMore="/posts/creating-a-technology-focused-website-with-jekyll"
            >
              Using static sites is a great way to build in speed an reliability on a
              website that does not require constant updates. Save money on hosting and
              maintenance.
            </PostCard>
          </Col>
          <Col lg="4" className="mb-5">
            <PostCard
              img={imgB2}
              preTitle="Oct 2, 2020"
              title="Selling direct from their website saved money!"
              readMore="/posts/selling-direct-from-their-website-saved-money"
            >
              When fusion brands wanted to sell tickets to their products online they first went to Amazon.
              They found that selling directly on the website
              made tickets far more affordable for clients.
            </PostCard>
          </Col>
          <Col lg="4" className="mb-5">
            <PostCard
              img={imgB3}
              preTitle="September 20, 2020"
              title="The neighborhood map allows for lot reservations during COVID"
              readMore="/posts/the-neighborhood-map-allows-for-lot-reservations-during-covid"
            >
              Their 3 new neighborhood maps now allow users to see what their future backyard looks
              like directly from the website. It even allows them to reserve a lot if interested.
            </PostCard>
          </Col>
          <Col lg="4" className="mb-5">
            <PostCard
              img={imgB4}
              preTitle="September 12, 2020"
              title="Microsites and how they can improve conversion"
              readMore="/posts/microsites-and-how-they-can-improve-conversion"
            >
              This Mother's Day Mann's Jewelers wanted to do something special.
            </PostCard>
          </Col>
          <Col lg="4" className="mb-5">
            <PostCard
              img={imgB5}
              preTitle="September 11, 2020"
              title="Morrell Commercial showcasing decades of development"
              readMore="/posts/morrell-commercial-showcasing-decades-of-development"
            >
              With properties spread over 3 states Morrell Commercial wanted to show
              the high quality of their spaces.
            </PostCard>
          </Col>
          <Col lg="4" className="mb-5">
            <PostCard
              img={imgB6}
              preTitle="September 9, 2020"
              title="Helping teens succeed during the COVID period"
              readMore="/posts/helping-teens-succeed-during-the-covid-period"
            >
              During these challenging times. My In Control wanted to allow students
              to request help directly from the website.
            </PostCard>
          </Col>
          <Col lg="4" className="mb-5">
            <PostCard
              img={imgB7}
              preTitle="March 9, 2021"
              title="9 Wordpress Plugins for Headless NextJS"
              readMore="/posts/9-wordpress-plugins-you-need-for-headless-nextjs"
            >
              Setting up a great headless website using NextJS and Wordpress is much easier with these 9 plugins.
            </PostCard>
          </Col>
        </Row>
        <Row>
          <Link href="/posts/buy-charlottesville-made-gifts-this-holiday">
            <a>
              <p>Buy Charlottesville Made Gifts this Holiday Season Online</p>
            </a>
          </Link>
        </Row>
          {/*
        <Box className="d-flex justify-content-center" mt={4}>
          <Pagination>
            <PageItem>
              <FaAngleLeft />
            </PageItem>
            <PageItem>1</PageItem>
            <PageItem>2</PageItem>
            <PageItem>3</PageItem>
            <PageItem>...</PageItem>
            <PageItem>9</PageItem>
            <PageItem>
              <FaAngleRight />
            </PageItem>
          </Pagination>
        </Box> */}
      </Container>
    </Section>
  </>
);

export default BlogList;
