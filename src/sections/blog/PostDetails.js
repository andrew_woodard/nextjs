import React from "react";
import styled from "styled-components";

import { Box, Badge } from "../../components/Core";

import imgB1 from "../../assets/image/jpeg/integritassmall.jpg";
import iconQuote from "../../assets/image/png/quote-icon.png";

const Post = styled(Box)`
  overflow: hidden;
  font-size: 1rem;

  h2,
  h3,
  h4,
  h5,
  h6,
  p,
  blockquote,
  pre,
  ul,
  ol {
    margin-bottom: 1.25rem;
    &:last-child {
      margin-bottom: 0;
    }
  }

  h2,
  h3,
  h4,
  h5,
  h6 {
    margin-top: 2.25rem;
    margin-bottom: 1.25rem;
    color: ${({ theme }) => theme.colors.heading} !important;
  }
  ol li {
    list-style-type: decimal;
  }

  ul li {
    list-style-type: disc;
  }

  blockquote {
    margin-bottom: 1.25rem;
    padding-left: 50px;
    position: relative;
    color: ${({ theme }) => theme.colors.text} !important;
    font-size: 20px;
    &::after {
      content: url(${iconQuote});
      display: inline-block;
      min-width: 28px;
      max-width: 28px;
      margin-top: 8px;
      margin-right: 23px;
      position: absolute;
      top: 0;
      left: 0;
    }
  }
  img,
  iframe,
  video {
    max-width: 100%;
    margin-bottom: 2rem;
    display: block;
    object-fit: cover;
  }
`;

const BadgePost = ({ children }) => (
  <Badge
    mr={3}
    mb={3}
    bg="#eae9f2"
    color="#696871"
    fontSize="16px"
    px={3}
    py={2}
  >
    {children}
  </Badge>
);

const PostDetails = () => (
  <>
    {/* <!-- Blog section --> */}
    <Post>
      <div>
        <img src={imgB1} alt="" />
      </div>
      <div>
        <p>
          Work smarter not harder. That is the lesson learned after many years of working on
          wordpress websites and fighting the good fight to keep them FAST. The problem with
          old school wordpress websites is that they grow and slow. You need a plugin one day
          and the next day your entire website is a bit slower.
        </p>
        <p>
          Several years ago I was introduced to Jekyll, my first static site builders, and I saw
          the benefits of lower maintenance and overhead that could come from building more Websites
          in that way.
        </p>
        <p>
          In 2019 I was introduced to Gatsby and NextJS and now I see even greater benefits that can
          come from working smarter instead of harder.
        </p>
        <p>
          {" "}
          The real time savings that you get with using a React based static site builder is to speed
          up development time. There are no servers to spin up when you use <a href="https://vercel.com">Vercel</a>
        and there are far fewer bugs to deal with when you website is statically generated.
        </p>
        <h3>Key Takeaway: Spend time on creating content, not keeping it from crashing.</h3>
      </div>
    </Post>
    <Box className="d-flex" mt={4}>
      <BadgePost>Freelance</BadgePost>
      <BadgePost>Design</BadgePost>
      <BadgePost>Earning</BadgePost>
      <BadgePost>Marketing</BadgePost>
      <BadgePost>Work</BadgePost>
    </Box>
  </>
);

export default PostDetails;
