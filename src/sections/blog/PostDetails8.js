import React from "react";
import styled from "styled-components";

import { Box, Badge } from "../../components/Core";

import iconQuote from "../../assets/image/png/quote-icon.png";
import imgC1 from "../../assets/image/jpeg/cinsider.jpg";

const Post = styled(Box)`
  overflow: hidden;
  font-size: 1rem;

  h2,
  h3,
  h4,
  h5,
  h6,
  p,
  blockquote,
  pre,
  ul,
  ol {
    margin-bottom: 1.25rem;
    &:last-child {
      margin-bottom: 0;
    }
  }

  h2,
  h3,
  h4,
  h5,
  h6 {
    margin-top: 2.25rem;
    margin-bottom: 1.25rem;
    color: ${({ theme }) => theme.colors.heading} !important;
  }
  ol li {
    list-style-type: decimal;
  }

  ul li {
    list-style-type: disc;
  }

  blockquote {
    margin-bottom: 1.25rem;
    padding-left: 50px;
    position: relative;
    color: ${({ theme }) => theme.colors.text} !important;
    font-size: 20px;
    &::after {
      content: url(${iconQuote});
      display: inline-block;
      min-width: 28px;
      max-width: 28px;
      margin-top: 8px;
      margin-right: 23px;
      position: absolute;
      top: 0;
      left: 0;
    }
  }
  img,
  iframe,
  video {
    max-width: 100%;
    margin-bottom: 2rem;
    display: block;
  }
`;

const BadgePost = ({ children }) => (
  <Badge
    mr={3}
    mb={3}
    bg="#eae9f2"
    color="#696871"
    fontSize="16px"
    px={3}
    py={2}
  >
    {children}
  </Badge>
);

const PostDetails = () => (
  <>
    {/* <!-- Blog section --> */}
    <Post>
      <div>
        <img src={imgC1} alt="Charlottesville Insider Website" />
      </div>
      <div>

        <h3>
          These are the top 9 plugins that are super helpful in building a NextJS powered headless site using Wordpress.
        </h3>
        <ul>
          <li>1) WP GraphQL - For getting your data over to NextJS.</li>
          <li>2) Advanced Custom Fields - To create custom fields associated with your posts.</li>
          <li>3) Custom Post Type UI - To create custom post types.</li>
          <li>4) Nullify empty fields for ACF - To keep undefined fields from messing with your builds.</li>
          <li>5) Vercel Deploy Hooks - To push your website to vercel.</li>
          <li>6) WPGraphQL Custom Post Type UI - Get graphql data over to </li>
          <li>7) WPGraphQL for Advanced Custom Fields - Get your custom fields over to NextJS.</li>
          <li>8) Yoast SEO - To give you those useful SEO fields.</li>
          <li>9) Add WPGraphQL SEO - To get your SEO data over to NextJS.</li>
        </ul>
      </div>
    </Post>
  </>
);

export default PostDetails;
