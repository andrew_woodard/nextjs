import React from "react";
import styled from "styled-components";
import { Container, Row, Col } from "react-bootstrap";

import { Title, Section, Box, Text } from "../../components/Core";

import imgContent1 from "../../assets/image/jpeg/easy-image-2-1.jpg";
import imgContent2 from "../../assets/image/jpeg/easy-image-2-2.jpg";

import Lottie from "lottie-react";
import webdesign from "../../assets/lottie/webdesign.json";


const ContentImg = styled(Box)`
  box-shadow: ${({ theme }) => `0 42px 54px ${theme.colors.shadow}`};
  border-radius: 10px;
  overflow: hidden;
  max-height: 515px;
`;

const Content = () => (
  <>
    <Section>
      <Container>
        <Row className="justify-content-center pb-4">
          <Col lg="6">
            <Title variant="hero">
              A huge factor that Google measures your website on is customer experience, which in my opinion every business is measured on.
            </Title>
          </Col>
          <Col lg="6" className="pl-lg-5">
          <Lottie animationData={webdesign} />
            <Text>
              That is where my background comes in to play. Prior to starting Spark Fire 6 years ago I worked for IEC Electronics, a contract electronics manufacturer, developing business analytics for a large company that produced over 100 million dollars in electronics per year. I developed the skills to help accomplish this from my years in business school at the Simon School of business in Rochester NY, which is known for a focus on finance and skills with numbers.</Text>
<Text>My approach is to create content that can focus on a KPI, Key Process Indicator, for your company. In many cases this KPI is online sales, in some it is contact form fills, and in some simply the time on site for visitors.</Text>
<Text>We create content that pulls potential clients in and leads them to the final destination of reaching out to you. The wonderful thing about online marketing is that everything is measured. From the amount of time a person spends on each of your web pages to the location the make access from, you can track your clients progress.
            </Text>
          </Col>
        </Row>
        {/*
        <Row className="mt-5">
          <Col lg="4" sm="5" className="mb-4 ">
            <ContentImg>
              <img src={imgContent1} alt="" className="img-fluid" />
            </ContentImg>
          </Col>
          <Col lg="8" sm="7" className="mb-4">
            <ContentImg>
              <img src={imgContent2} alt="" className="img-fluid" />
            </ContentImg>
          </Col>
        </Row>
        */}
      </Container>
    </Section>
  </>
);

export default Content;
