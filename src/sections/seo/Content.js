import React from "react";
import styled from "styled-components";
import { Container, Row, Col } from "react-bootstrap";

import { Title, Section, Box, Text } from "../../components/Core";

import imgContent1 from "../../assets/image/jpeg/easy-image-2-1.jpg";
import imgContent2 from "../../assets/image/jpeg/easy-image-2-2.jpg";

import Lottie from "lottie-react";
import webdesign from "../../assets/lottie/webdesign.json";


const ContentImg = styled(Box)`
  box-shadow: ${({ theme }) => `0 42px 54px ${theme.colors.shadow}`};
  border-radius: 10px;
  overflow: hidden;
  max-height: 515px;
`;

const Content = () => (
  <>
    <Section>
      <Container>
        <Row className="justify-content-center pb-4">
          <Col lg="6">
            <Title variant="hero">
              Ranking for a keyword is a long term war for position. Hundreds of factors come together to position you.
            </Title>
          </Col>
          <Col lg="6" className="pl-lg-5">
       {/*   <Lottie animationData={webdesign} /> */}
            <Text>
              My approach for Search Engine Optimization is to focus a company on what would make their business and content as relevant as possible to potential clients.</Text><br></br>
          <Text>Sometimes the things that can impact ranking the most are cheap or free for a business to do. Asking clients for quality reviews and making sure website pages load quickly can have a huge impact.</Text>
          </Col>
        </Row>
        {/*
        <Row className="mt-5">
          <Col lg="4" sm="5" className="mb-4 ">
            <ContentImg>
              <img src={imgContent1} alt="" className="img-fluid" />
            </ContentImg>
          </Col>
          <Col lg="8" sm="7" className="mb-4">
            <ContentImg>
              <img src={imgContent2} alt="" className="img-fluid" />
            </ContentImg>
          </Col>
        </Row>
        */}
      </Container>
    </Section>
  </>
);

export default Content;
