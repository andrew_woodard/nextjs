import React from "react";
import styled from "styled-components";
import { Container, Row, Col } from "react-bootstrap";

import { Title, Section, Box, Text } from "../../components/Core";

import imgContent1 from "../../assets/image/jpeg/easy-image-2-1.jpg";
import imgContent2 from "../../assets/image/jpeg/easy-image-2-2.jpg";

const ContentImg = styled(Box)`
  box-shadow: ${({ theme }) => `0 42px 54px ${theme.colors.shadow}`};
  border-radius: 10px;
  overflow: hidden;
  max-height: 515px;
`;

const Content = () => (
  <>
    <Section>
      <Container>
        <Row className="justify-content-center pb-4">
          <Col lg="6">
            <Title variant="hero">
              The wonderful thing about digital as a marketing platform is that so much can be measured.
            </Title>
          </Col>
          <Col lg="6" className="pl-lg-5">
            <Text>
              Analytics and Heat Maps can tell us a huge amount about how your clients interact with our advertising
            and website online. Today we can see everything from how long a user spends on our website, to what they look at buying,
        and where they come from when they arrive at the website. Using these analytics we can understand their decision making. As
      an example I know that my potential clients come to this website looking for help improving their online presence. I know that they
    find the website either from one of my client websites (referral) or by doing a google search (organic). Using this knowledge I known
  how to augment this traffic using paid for advertising. Using paid for advertising I can greatly increase my reach (The potential clients that see my website).  </Text>
          </Col>
        </Row>
        {/*
        <Row className="mt-5">
          <Col lg="4" sm="5" className="mb-4 ">
            <ContentImg>
              <img src={imgContent1} alt="" className="img-fluid" />
            </ContentImg>
          </Col>
          <Col lg="8" sm="7" className="mb-4">
            <ContentImg>
              <img src={imgContent2} alt="" className="img-fluid" />
            </ContentImg>
          </Col>
        </Row>
        */}
      </Container>
    </Section>
  </>
);

export default Content;
