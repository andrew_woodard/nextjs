import React from "react";
import styled from "styled-components";
import { Container } from "react-bootstrap";

import { Title, Section, Button } from "../../components/Core";

const Table = styled.table`
  thead,
  tr,
  tbody {
    display: block;
  }
  thead tr .sin-title-box {
    padding: 0 20px;
    height: 60px;
    color: #19191b;
    font-size: 18px;
    font-weight: 700;
    letter-spacing: -0.56px;
  }
  .title-col {
    min-width: 125px;
    width: 50%;
  }
  @media (min-width: 767px) {
    .title-col {
      min-width: 290px;
    }
}
@media (min-width: 767px) {
  .basic-col {
    width: calc(90vw - 290px);
  }
  .title-col {
    max-width: 290px;
  }
}
  .basic-col,
  .standard-col,
  .premium-col,
  .ent-col {
    min-width: 195px;
  }

  .colored-row {
    background: #f7f7fb;
    border-top: 2px solid #fff;
    &:first-child {
      border-top: 0;
    }
    .sin-box.title-col {
      color: #19191b;
      font-weight: 700;
    }
    .sin-box {
      position: relative;
      padding: 0 20px;
      height: 65px;
      color: #696871;
      font-size: 18px;
      font-weight: 300;
      letter-spacing: -0.56px;
      .table-icon {
        font-size: 20px;
        &.neg {
          color: #f04037;
        }
        &.pos {
          color: #67d216;
        }
      }
      &:before {
        content: "";
        background: #fff;
        height: 100%;
        top: 0;
        left: 0;
        position: absolute;
        width: 1px;
      }
    }
  }
`;

const ButtonSm = styled(Button)`
  width: 167px;
  min-width: auto;
  height: 50px;
  border-radius: 5px;
  border: 1px solid #eae9f2;
  background-color: #ffffff;
  color: ${({ theme }) => theme.colors.secondary};
  font-size: 18px;
  font-weight: 500;
  letter-spacing: -0.56px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  padding: 0.375rem 0.75rem;
  line-height: 1.5;
  text-align: center;
  vertical-align: middle;
  user-select: none;
  &:hover,
  &:focus {
    border: ${({ theme }) => `1px solid ${theme.colors.secondary}`} !important;
    background-color: ${({ theme }) => `${theme.colors.secondary}`} !important;
    color: #fff;
  }
`;

const PricingTable2 = () => {
  return (
    <>
      <Section className="pt-0">
        <Container>
          <div
            css={`
              overflow: auto;
              padding-bottom: 30px;
            `}
          >
            <Table>
              <thead>
                <tr>
                  <th scope="col" className="title-col sin-title-box">
                    Company Name
                  </th>
                  <th scope="col" className="basic-col sin-title-box">
                    What products they are known for
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr className="colored-row">
                  <th scope="row" className="title-col sin-box">
                    <a href="https://blanccreatives.com/" target="_blank">Blanc Creatives</a>
                  </th>
                  <td className="basic-col sin-box">Carbon Steel Cookware</td>
                </tr>
                <tr className="colored-row">
                  <th scope="row" className="title-col sin-box">
                    <a href="https://casparionline.com" target="_blank">Caspari</a>
                  </th>
                  <td className="basic-col sin-box">A published or art on paper.</td>
                </tr>
                <tr className="colored-row">
                  <th scope="row" className="title-col sin-box">
                    <a href="https://cardboardsfari.com" target="_blank">Cardboard Safari</a>
                  </th>
                  <td className="basic-col sin-box">Decor for fun.</td>
                </tr>
                <tr className="colored-row">
                  <th scope="row" className="title-col sin-box">
                    <a href="https://www.chickapig.com/" target="_blank">Chickapig</a>
                  </th>
                  <td className="basic-col sin-box">Fun boardgame.</td>
                </tr>
                <tr className="colored-row">
                  <th scope="row" className="title-col sin-box">
                    <a href="https://www.monolithknives.com/collections/knives-in-stock" target="_blank">Monolith Knives</a>
                  </th>
                  <td className="basic-col sin-box">Bespoke knives.</td>
                </tr>
                <tr className="colored-row">
                  <th scope="row" className="title-col sin-box">
                    <a href="https://www.etsy.com/shop/victoriahorner/">Victoria Horner</a>
                  </th>
                  <td className="basic-col sin-box">Handbags</td>
                </tr>
                <tr className="colored-row">
                  <th scope="row" className="title-col sin-box">
                    <a href="https://yamamountaingear.com/" target="_blank">Yama Mountain Gear</a>
                  </th>
                  <td className="basic-col sin-box">Backpacks, Tents, and Camping Gear.</td>
                </tr>
                <tr className="colored-row">
                  <th scope="row" className="title-col sin-box">
                    <a href="http://wileybelts.com/" target="_blank">Wiley Belts</a>
                  </th>
                  <td className="basic-col sin-box">Leather Belts.</td>
                </tr>
              </tbody>
            </Table>
          </div>
        </Container>
      </Section>
    </>
  );
};

export default PricingTable2;
