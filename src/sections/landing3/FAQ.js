import React from "react";
import styled from "styled-components";
import { Container, Row, Col } from "react-bootstrap";

import { Title, Section, Box, Text } from "../../components/Core";

const SectionStyled = styled(Section)``;

const FaqCard = ({ title, children, ...rest }) => (
  <Box {...rest}>
    <Title variant="card" mb={4} fontSize="24px" fontWeight="900" color="light">
      {title}
    </Title>
    <Text variant="small" color="lightShade">
      {children}
    </Text>
  </Box>
);

const FAQ = () => (
  <>
    <SectionStyled bg="dark">
      <Container>
        <Row className="justify-content-center">
          <Col xl="7" lg="8" md="9">
            <div className="text-center">
              <Title color="light">
                Frequently
                <br className="d-none d-sm-block" /> Asked Question
              </Title>
              <Text color="lightShade">

              </Text>
            </div>
          </Col>
        </Row>
        <Row className="pt-4">
          <Col lg="6" className="mt-5">
            <FaqCard title="How much should it cost to build a website in 2020?">
              Short Answer: $1,000 to $15,000 | Long Answer: It depends on how many pages,
              how much customization, and how many additional features that you want to have.
            </FaqCard>
          </Col>
          <Col lg="6" className="mt-5">
            <FaqCard title="How important is Search Engine Optimization?">
              Organic traffic is the free traffic you get from search engines like Google. By
              focusing your content on relating to searches that your potential clients make your
              can gain that valuable traffic. Search Engine Optimization leads to this organic traffic.
            </FaqCard>
          </Col>
          <Col lg="6" className="mt-5">
            <FaqCard title="What are my options for selling my products online?">
              There are dozens of options including full fledged platforms like Shopify,
              embeddable options like Stripe and connectable services like Fareharbor.
            </FaqCard>
          </Col>
          <Col lg="6" className="mt-5">
            <FaqCard title="How can I best advertise online?">
              Know your audience and where they spend their time online. Excite them about the experience
              your product or service can bring and how it can fit into their lives.
            </FaqCard>
          </Col>
          <Col lg="6" className="mt-5">
            <FaqCard title="How important is it for me to have a blog?">
              It is important for you to have quality content. This content can come in the form of posts That
              are time relevant or pages that are not. The goal is to answer questions, give experiences and
              make your content worth the users time.
            </FaqCard>
          </Col>
          <Col lg="6" className="mt-5">
            <FaqCard title="What to watch for in my websites analytics?">
              Decide what a successful visit is and if it relates to a conversion. A conversion can be a sale,
              signup, message or even a chance to show off your product or service to that potential client. Track
              those conversions and try to increase their prevelance.
            </FaqCard>
          </Col>

          <Col lg="12" className="">
            <Box className="text-center" pt={5}>
              <Text variant="small" color="lightShade">
                Do you have another question?{" "}
                <a
                  href="/contact"
                  css={`
                    color: white!important;
                  `}
                >
                  Contact us here
                </a>
              </Text>
            </Box>
          </Col>
        </Row>
      </Container>
    </SectionStyled>
  </>
);

export default FAQ;
