import React from "react";
import styled from "styled-components";
import { Container, Row, Col } from "react-bootstrap";

import { Title, Section, Box, Text, Badge } from "../../components/Core";

import imgShape from "../../assets/image/png/content1-img-small.png";
import imgContent from "../../assets/image/jpeg/themarket.jpg";

const SectionStyled = styled(Section)`
  border-top: 1px solid #242427;
`;

const ShapeBox = styled(Box)`
  position: absolute;
  top: -70px;
  left: -20px;
  z-index: 2;
`;

const Content1 = () => (
  <>
    <SectionStyled bg="dark">
      <Container>
        <Row className="justify-content-center align-items-center">
          <Col lg="6" className="my-5 my-lg-0">
            <div className="">
              <Box
                className=" "
                data-aos="zoom-out"
                data-aos-duration="750"
                data-aos-once="true"
                data-aos-delay="500"
              >
                <img src={imgContent} alt="" className="img-fluid" />
              </Box>

            </div>
          </Col>
          <Col
            md="10"
            lg="6"
            className="mt-3 mt-lg-0"
            data-aos="fade-right"
            data-aos-duration="750"
            data-aos-once="true"
          >
            <Badge bg="secondary">You need a great website!</Badge>
            <div className="mt-4">
              <Title color="light">
                Telling the story<br className="d-none d-md-block" /> of your business
              </Title>
              <Text color="lightShade">
                Your website functions as the first impression for your business. It gives you
                the great opportunity to show your work in just the way you want it. Your website
                is a reflexion of how you want to run your business.

              </Text>
            </div>
          </Col>
        </Row>
      </Container>
    </SectionStyled>
  </>
);

export default Content1;
