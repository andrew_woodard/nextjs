import React from "react";
import { rgba } from "polished";
import { Container, Row, Col } from "react-bootstrap";
import Link from "next/link";

import { Title, Section, Box, Text } from "../../components/Core";

const FeatureCard = ({
  color = "primary",
  iconName,
  title,
  children,
  ...rest
}) => (
  <Box pt="15px" px="30px" borderRadius={10} mb={4} {...rest}>
    <Box
      size={69}
      borderRadius="50%"
      color={color}
      fontSize="28px"
      className="d-flex justify-content-center align-items-center mx-auto"
      css={`
        background-color: ${({ theme, color }) =>
          rgba(theme.colors[color], 0.1)};
      `}
    >
      <i className={`icon ${iconName}`}></i>
    </Box>
    <div className="text-center">
      <Text
        color="light"
        as="h3"
        fontSize={4}
        fontWeight={500}
        letterSpacing={-0.75}
        my={3}
      >
        {title}
      </Text>
      <Text fontSize={2} lineHeight={1.75} color="lightShade">
        {children}
      </Text>
    </div>
  </Box>
);

const Feature = () => (
  <>
    {/* <!-- Feature section --> */}
    <Section className="position-relative pb-0" bg="dark">
      <Container>
      <Row className="justify-content-center">
        <Col md="8" lg="8" xl="8">
          <div className="text-center pb-8">
            <Title color="light">Services that can help your business grow</Title>
              <Text color="lightShade">

              </Text>
          </div>
        </Col>
      </Row>
        <Row className="align-items-center justify-content-center">
          <Col sm="6" md="5" lg="4" className="mt-3 mt-lg-5">
            <Link href="/web-development">
              <a>
            <FeatureCard
              color="secondary"
              iconName="icon-sidebar-2"
              title="Company Websites"
            >
              Tell the story of your business. Helping you grow your community.
            </FeatureCard>
            </a>
            </Link>
          </Col>

          <Col sm="6" md="5" lg="4" className="mt-3 mt-lg-5">
            <Link href="/online-stores">
              <a>
            <FeatureCard
              color="primary"
              iconName="icon-layout-11"
              title="Online Stores"
            >
              Sell your product or service with an e-commerce store or appointment scheduler.
            </FeatureCard>
          </a>
        </Link>
          </Col>

          <Col sm="6" md="5" lg="4" className="mt-3 mt-lg-5">
            <Link href="/online-marketing">
              <a>
            <FeatureCard
              color="secondary"
              iconName="icon-airplane-2"
              title="Online Marketing"
            >
              Reach your target market with ads on Google Search, Ad Networks, Facebook and even Video
            </FeatureCard>
          </a>
        </Link>
          </Col>
        </Row>
      </Container>
    </Section>
  </>
);

export default Feature;
