import React from "react";
import Link from "next/link";
import { Container, Row, Col } from "react-bootstrap";

import PageWrapper from "../../components/PageWrapper";
import { Section, Title, Text, Box } from "../../components/Core";

import PostDetails from "../../sections/blog/PostDetails7";
import Comments from "../../sections/blog/Comments";
import Sidebar from "../../sections/blog/Sidebar";
import { NextSeo } from 'next-seo';
import PricingTable2 from "../../sections/blog/PricingTable2";

const BlogDetails = () => {
  return (
    <>
      <PageWrapper footerDark>
        <NextSeo
          title="Buy Charlottesville Made Gifts this Holiday Season"
          description="Every holiday season I like to buy local first. Help out your Charlottesville community by looking at some of the great makers right here in town."
          canonical="https://www.canonical.ie/"
      openGraph={{
        url: 'https://sparkfirewebdesign.com',
        title: 'Buy Charlottesville Made Gifts this Holiday Season',
        description: 'Every holiday season I like to buy local first. Help out your Charlottesville community by looking at some of the great makers right here in town',
        images: [
          {
            url: '../../assets/image/jpeg/jamsbydan.jpg',
            width: 800,
            height: 600,
            alt: 'Charlottesville Buy Local Gifts.',
          },
        ],
        site_name: 'Spark Fire Web Design',
      }}
        />
        <Section className="pb-0">
          <div className="pt-5"></div>
          <Container>
            <Row className="justify-content-center text-center">
              <Col lg="12">
                <Title variant="hero">
                  Buy Charlottesville Made Gifts this Holiday Season Online{" "}
                  <br className="d-none d-lg-block" />
                </Title>
                <Box className="d-flex justify-content-center">
                  <Text mr={3}>
                    <Link href="/">
                      <a>October 19, 2020</a>
                    </Link>
                  </Text>
                  <Text mr={3}>
                    <Link href="/">
                      <a>Websites</a>
                    </Link>
                  </Text>
                  <Text>
                    <Link href="/">
                      <a>Andrew Woodard</a>
                    </Link>
                  </Text>
                </Box>
              </Col>
            </Row>
          </Container>
        </Section>
        <Section className="pb-0">
          <Container>
            <Row>
              <Col lg="12" className="mb-5">
                <PostDetails />
                <br></br>
                <PricingTable2 />
              </Col>
            </Row>
          </Container>
        </Section>
      </PageWrapper>
    </>
  );
};
export default BlogDetails;
