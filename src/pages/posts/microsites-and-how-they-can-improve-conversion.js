import React from "react";
import Link from "next/link";
import { Container, Row, Col } from "react-bootstrap";

import PageWrapper from "../../components/PageWrapper";
import { Section, Title, Text, Box } from "../../components/Core";

import PostDetails from "../../sections/blog/PostDetails4";
import Comments from "../../sections/blog/Comments";
import Sidebar from "../../sections/blog/Sidebar";
import { NextSeo } from 'next-seo';

const BlogDetails = () => {
  return (
    <>
      <PageWrapper footerDark>
        <NextSeo
          title=""
          description=""
          canonical="https://www.canonical.ie/"
      openGraph={{
        url: 'https://sparkfirewebdesign.com',
        title: '',
        description: '',
        images: [
          {
            url: '../assets/image/jpeg/myincontrolsmall.jpg',
            width: 800,
            height: 600,
            alt: 'COVID Teen program with interactive forms.',
          },
        ],
        site_name: 'Spark Fire Web Design',
      }}
        />
        <Section className="pb-0">
          <div className="pt-5"></div>
          <Container>
            <Row className="justify-content-center text-center">
              <Col lg="12">
                <Title variant="hero">
                  Microsites and how they can improve conversion.{" "}
                  <br className="d-none d-lg-block" />
                </Title>
                <Box className="d-flex justify-content-center">
                  <Text mr={3}>
                    <Link href="/">
                      <a>October 4, 2020</a>
                    </Link>
                  </Text>
                  <Text mr={3}>
                    <Link href="/">
                      <a>Websites</a>
                    </Link>
                  </Text>
                  <Text>
                    <Link href="/">
                      <a>Andrew Woodard</a>
                    </Link>
                  </Text>
                </Box>
              </Col>
            </Row>
          </Container>
        </Section>
        <Section className="pb-0">
          <Container>
            <Row>
              <Col lg="12" className="mb-5">
                <PostDetails />
              </Col>
              <Col lg="0" className="">
                <Sidebar />
              </Col>
            </Row>
          </Container>
        </Section>
      </PageWrapper>
    </>
  );
};
export default BlogDetails;
