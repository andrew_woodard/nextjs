import React from "react";
import Hero from "../sections/landing3/Hero";
import VideoCTA from "../sections/landing3/VideoCTA";
import Feature from "../sections/landing3/Feature";
import Content1 from "../sections/landing3/Content1";
import Content2 from "../sections/landing3/Content2";
import Content3 from "../sections/landing3/Content3";
import FAQ from "../sections/landing3/FAQ";
import CTA from "../sections/landing3/CTA";

import PageWrapper from "../components/PageWrapper";
import { NextSeo, LocalBusinessJsonLd } from 'next-seo';


const LandingPage3 = () => {
  return (
    <>
      <PageWrapper headerDark footerDark>
        <NextSeo
          title="Charlottesville Web Developer - Spark Fire Web Design"
          description="Charlottesville Web Designer, Developer, and Marketer. Working with businesses to compete online. Website design and development company in Charlottesville, VA ✅"
          canonical="https://sparkfirewebdesign.com"
      openGraph={{
        url: 'https://sparkfirewebdesign.com',
        title: 'Charlottesville Web Developer',
        description: 'Charlottesville Web Design, Developer, and Marketer. Working with businesses to compete online. Website design and development company in Charlottesville, VA.',
        images: [
          {
            url: 'https://sparkfirewebdesign.com/assets/image/jpeg/morrellbuilderssmall.jpg',
            width: 800,
            height: 600,
            alt: 'Spark Fire Web Design Charlottesville.',
          },
        ],
        site_name: 'Spark Fire Web Design',
      }}
        />
        <LocalBusinessJsonLd
  type="Organization"
  id="https://sparkfirewebdesign.com"
  name="Spark Fire Web Design"
  description="Charlottesville Web Design, Developer, and Marketer. Working with businesses to compete online. Website design and development company in Charlottesville, VA."
  url="https://sparkfirewebdesign.com"
  telephone="+15855031962"
  address={{
    streetAddress: '2554 Holkham Drive',
    addressLocality: 'Charlottesville',
    addressRegion: 'VA',
    postalCode: '22901',
    addressCountry: 'US',
  }}
  images={[
    '/images/jpeg/oldmetro.jpg',
    '/images/jpeg/mwt.jpg',
    '/images/jpeg/purpledoor.jpg',
  ]}
  sameAs={[
    'https://www.facebook.com/sparkfirewebdesign/',
    'https://www.linkedin.com/company/5154058',
  ]}
  openingHours={[
    {
      opens: '09:00',
      closes: '17:00',
      dayOfWeek: [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',        
      ],
      validFrom: '2019-12-23',
      validThrough: '2025-04-02',
    },
  ]}
  areaServed={[
    {
      geoMidpoint: {
        latitude: '38.02930',
        longitude: '-78.47670',
      },
      geoRadius: '160934',
    }
  ]}
  action={{
    actionName: 'potentialAction',
    actionType: 'ReviewAction',
    target: 'https://www.google.com/search?q=sparkfire+web+design&oq=sparkfire+web+design&aqs=chrome..69i57j69i61j69i60j69i61.2002j0j7&sourceid=chrome&ie=UTF-8#',
  }}
/>

        <Hero /> 
        <Feature />
        <Content1 />
        <Content3 />
        <FAQ />
        <CTA />
      </PageWrapper>
    </>
  );
};
export default LandingPage3;
