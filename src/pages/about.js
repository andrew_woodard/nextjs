import React from "react";

import PageWrapper from "../components/PageWrapper";
import Hero from "../sections/common/Hero";
import Content from "../sections/about/Content";
import Team from "../sections/about/Team";
import CTA from "../sections/about/CTA";

import herosimage from "../assets/image/jpeg/1bday-1.jpg";
import { NextSeo } from 'next-seo';


const About = () => {
  return (
    <>
      <PageWrapper>
        <NextSeo
          title="About Andrew Woodard and Spark Fire Web Design"
          description="My name is Andrew Woodard. I have been working with business to help them grow my entire life."
          canonical="https://sparkfirewebdesign.com/about"
      openGraph={{
        url: 'https://sparkfirewebdesign.com/about',
        title: 'About Spark Fire Web Design',
        description: 'My name is Andrew Woodard. I have been working with business to help them grow my entire life.',
        images: [
          {
            url: { herosimage },
            width: 800,
            height: 600,
            alt: 'Andrew Woodard Owner of Spark Fire Web Design',
          },
        ],
        site_name: 'Spark Fire Web Design',
      }}
        />
        <Hero title="About Andrew" image={ herosimage }>
          My name is Andrew Woodard. I started Spark Fire Web Design to
          help business owners take control of their online presence. I
          continue to enjoy learning the most exciting new tools to help
           my clients compete. Tools that make their businesses run more
           smoothly and that make their clients happy.
           <br></br><br></br>
           My goal working with clients is to be as responsive as can be possible.
           You can always reach me on the phone and I'll always get back to you
           within an hour with an email reply. I'm here to answer your questions
           and if I don't know the answer then I will find it for you.
        </Hero>
        <Content />
        <CTA />
      </PageWrapper>
    </>
  );
};
export default About;
