import React from "react";

import PageWrapper from "../components/PageWrapper";
import Hero from "../sections/common/Hero";
import Content from "../sections/seo/Content";
import Team from "../sections/seo/Team";
import CTA from "../sections/seo/CTA";



import herosimage from "../assets/image/jpeg/seo-ranked.jpg?webp";
import { NextSeo } from 'next-seo';

const About = () => {
  return (
    <>
      <PageWrapper>
        <NextSeo
          title="SEO Expert Charlottesville VA - Search Engine Optimization"
          description="Grow your online presence with organic search engine optimization. Let me help you rank better."
          openGraph={{
            url: 'https://sparkfirewebdesign.com',
            title: 'Charlottesville Web Developer',
            description: 'Grow your online presence with organic search engine optimization. Let me help you rank better.',
            images: [
              {
                url: 'https://sparkfirewebdesign.com/assets/image/jpeg/inde.jpg',
                width: 800,
                height: 600,
                alt: 'SEO Expert Charlottesville.',
              },
            ],
            site_name: 'Spark Fire Web Design',
          }}
        />
        <Hero title="SEO Services" image={herosimage}>
          Competition for online rankings has become increasingly important as Google has been leading the search market. Spark Fire works with businesses to rank by focusing content, improving reputation, optimizing user experience, and forming relationships that create inlinks. 
        </Hero>
        <Content />
        <Team />
        <CTA />
      </PageWrapper>
    </>
  );
};
export default About;
