import React from "react";
import styled from "styled-components";
import { Container, Row, Col } from "react-bootstrap";

import { Title, Button, Section, Box, Text, Input } from "../components/Core";

import PageWrapper from "../components/PageWrapper";
import Contactf from "../components/Core/Contactf";

import Hero from "../sections/common/Hero";
import useForm from "../hooks/useForm";

import herosimage from "../assets/image/jpeg/cvilledowntown.jpg";
import { NextSeo } from 'next-seo';

const FormStyled = styled(Box)`
  position: relative;
  padding: 30px;
  background-color: ${({ theme }) => theme.colors.light};
`;

const Contact1 = () => {
  return (
    <>
      <PageWrapper footerDark>
        <NextSeo
          title="Contact Spark Fire Web Design - Charlottesville VA"
          description="Learn about how I can improve your web presence, grow your sales, and make your business look competitive."
          canonical="https://sparkfirewebdesign.com/contact"
      openGraph={{
        url: 'https://sparkfirewebdesign.com/contact',
        title: 'Contact Spark Fire Web Design - Charlottesville VA',
        description: 'Learn about how I can improve your web presence, grow your sales, and make your business look competitive.',
        images: [
          {
            url: { herosimage },
            width: 800,
            height: 600,
            alt: 'Andrew Woodard Owner of Spark Fire Web Design',
          },
        ],
        site_name: 'Spark Fire Web Design',
      }}
        />
      <Hero title="Get in touch today" image={ herosimage }>
          Let's talk about creating a better web presence for your company. Grow revenue and create a better customer experience.
          Contact me below to learn about tools that can help your company succeed in 2020 and beyond.
        </Hero>
        <Section>
          <Container>
            <Row className="align-items-center">
              <Col
                lg={5}
                className="offset-lg-1 order-lg-2 mt-5 mt-lg-0 pt-lg-5"
              >
                <Box className="mb-5">
                  <Title variant="card" fontSize="24px">
                    Call us
                  </Title>
                  <Text>+1-434-264-1932‬</Text>
                  <Text>+1-585-503-1962</Text>
                </Box>
                <Box className="mb-5">
                  <Title variant="card" fontSize="24px">
                    Email us
                  </Title>
                  <Text>andrew@sparkfirewebdesign.com</Text>
                </Box>
                <Box className="mb-5">
                  <Title variant="card" fontSize="24px">
                    Contact us
                  </Title>
                  <Text>Charlottesville VA</Text>
                  <Text>Rochester NY</Text>
                </Box>
              </Col>
              <Col lg="6">
                <FormStyled>
                  <Box mb={5}>
                    <Title>I'm looking forward to hearing about your business.</Title>
                  </Box>


                  <Contactf></Contactf>

                </FormStyled>
              </Col>
            </Row>
          </Container>
        </Section>
      </PageWrapper>
    </>
  );
};
export default Contact1;
