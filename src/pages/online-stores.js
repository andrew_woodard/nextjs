import React from "react";

import PageWrapper from "../components/PageWrapper";
import Hero from "../sections/common/Hero";
import Content from "../sections/online-stores/Content";
import Team from "../sections/online-stores/Team";
import CTA from "../sections/online-stores/CTA";

import herosimage from "../assets/image/png/manns.png";
import { NextSeo } from 'next-seo';

const About = () => {
  return (
    <>
      <PageWrapper>
        <NextSeo
          title="Online Stores Charlottesville VA - E-Commerce Consultant and Expert"
          description="Sell your product or service online. With over 6 years of experience helping businesses to make a profit with online sales."
        />
        <Hero title="E-Commerce - Online Stores" image={herosimage}>
          Make your product or service available online. Using tools like Shopify, Fareharbor, Calendly, Woocommerce and Stripe
          to help increase your company sales. Work with me as a consultant or hire me to build you a full E-Commerce solution.
        </Hero>
        <Content />
        <Team />
        <CTA />
      </PageWrapper>
    </>
  );
};
export default About;
