import React from "react";

import PageWrapper from "../components/PageWrapper";
import Hero from "../sections/common/Hero";
import Content from "../sections/web-development/Content";
import Team from "../sections/web-development/Team";
import CTA from "../sections/web-development/CTA";



import herosimage from "../assets/image/jpeg/fast-websites.jpg?webp";
import { NextSeo } from 'next-seo';

const About = () => {
  return (
    <>
      <PageWrapper>
        <NextSeo
          title="Web Developer Charlottesville VA - Web Design - Development"
          description="Web Design and Development to help business compete in a more digital world."
        />
        <Hero title="Web Development" image={herosimage}>
          Creating websites your clients will enjoy and that bring your business greater sales.
          Using cutting edge technology to bring a superior experience. I work with a variety of
          Content Management system to make managing your website fun and easy. <a target="_blank" href="https://theindestudio.com">Kitchen Remodeling Website</a>
        </Hero>
        <Content />
        <Team />
        <CTA />
      </PageWrapper>
    </>
  );
};
export default About;
