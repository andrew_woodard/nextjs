import React from "react";

import PageWrapper from "../components/PageWrapper";
import Hero from "../sections/common/Hero";
import Content from "../sections/online-marketing/Content";
import Team from "../sections/online-marketing/Team";
import CTA from "../sections/online-marketing/CTA";

import herosimage from "../assets/image/jpeg/marketing.jpg";

const About = () => {
  return (
    <>
      <PageWrapper>
        <Hero title="Online Marketing" image={herosimage}>
          Online Advertising gets the word out about your product or service. Get precision
          targetting and a great return on every advertising dollar spent. As a web developer and
           marketer I understand how to use landing pages to get you the most out of your advertising. 
        </Hero>
        <Content />
        <Team />
        <CTA />
      </PageWrapper>
    </>
  );
};
export default About;
