import React from "react";
import styled from "styled-components";
import { color, space, typography, shadow } from "styled-system";

const Paragraph = styled.p`
  font-family: andrew;
  margin-bottom: 0;
  text-align: right;
  font-size: 44px;
  font-weight: 300;
  letter-spacing: 0px;
  line-height: 38px;
  color: black;
  ${space};
  ${typography};
  ${shadow};
`;

const ParagraphSmall = styled(Paragraph)`
  font-family: andrew;
  font-size: 44px;
  letter-spacing: 0px;
  text-align: right;
  line-height: 28px;
  color: black;
  ${space};
  ${typography};
  ${shadow};
`;

const Signature = ({ variant, ...props }) => {
  let TextRender;

  switch (variant) {
    case "small":
      TextRender = ParagraphSmall;
      break;
    default:
      TextRender = Paragraph;
  }

  return <TextRender color="text" {...props} />;
};

export default Signature;
