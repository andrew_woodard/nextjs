export const menuItems = [
  {
    name: "",
    label: "Home",
    isExternal: false,
  },
  {
    name: "about",
    label: "About",
    isExternal: false,
  },
  {
    name: "pages",
    label: "Services",
    items: [
      { name: "web-development", label: "Web Development" },
      { name: "online-stores", label: "Online Stores" },
      { name: "online-marketing", label: "Online Marketing" },
      { name: "seo", label: "Search Engine Optimization" },
    ],
  },
  {
    name: "work",
    label: "Work",
    isExternal: false,
  },
  {
    name: "contact",
    label: "Contact",
    isExternal: false,
  },
];
